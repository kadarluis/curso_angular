import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() cliked: EventEmitter<DestinoViaje>;

  constructor() {
    this.cliked = new EventEmitter();
  }

  ngOnInit() {
  }

  ir() {
    this.cliked.emit(this.destino);
    return false;
  }
}
